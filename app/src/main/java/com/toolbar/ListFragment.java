package com.toolbar;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Виталий on 15.12.2014.
 */
public class ListFragment extends Fragment {

    private ListView mListView;
    private View mHeader;
    private Toolbar mToolbar;
    private int mToolbarHeight;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragment = inflater.inflate(R.layout.list_fragment, null);
        mHeader = LayoutInflater.from(getActivity()).inflate(R.layout.list_fragment_header, null);
        mToolbar = (Toolbar) fragment.findViewById(R.id.toolbar);
        mToolbar.setTitle("Toolbar");
        mToolbar.setLogo(R.drawable.ic_launcher);
        mToolbar.post(new Runnable() {
            @Override
            public void run() {
                mToolbarHeight = mToolbar.getHeight();
            }
        });
        mListView = (ListView) fragment.findViewById(R.id.list);
        mListView.addHeaderView(mHeader);
        mListView.setAdapter(new ListAdapter());
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            int mLastScrollY = 0;

            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {}

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int scrollY = getScrollY();
                int translation;
                if (mLastScrollY - scrollY > 0) {
                    //скролл вниз
                    translation = mLastScrollY - scrollY > mToolbarHeight ? 0 : -mToolbarHeight + mLastScrollY - scrollY;
                    if (translation == 0)
                        mLastScrollY = scrollY + mToolbarHeight;

                } else {
                    //скролл вверх
                    translation = -scrollY < -mToolbarHeight ? -mToolbarHeight : -scrollY;
                    mLastScrollY = scrollY;
                }
                mToolbar.setTranslationY(translation);
            }
        });
        return fragment;
    }

    public int getScrollY() {
        View c = mListView.getChildAt(0);
        if (c == null)
            return 0;
        int firstVisiblePosition = mListView.getFirstVisiblePosition();
        int top = c.getTop();
        return -top + firstVisiblePosition * c.getHeight();
    }

    private class ListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return 100;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View item = LayoutInflater.from(getActivity()).inflate(R.layout.list_fragment_item, null);
            TextView itemText = (TextView) item.findViewById(R.id.text);
            itemText.setText("STUB!");
            return item;
        }
    }
}
